{-|
   Problem 31
   (**) Determine whether a given integer number is prime.

   Example in Haskell:
   > isPrime 7
   True
-}

-- Eratosphen' sieve
primes :: [Integer]
primes = 1 : sieve [2,3..] where
    sieve (p:ns) = p : (sieve $ filter (\n -> n `mod` p /= 0) ns)
    
isPrime :: Integer -> Bool
isPrime1 :: Integer -> Bool
isPrime2 :: Integer -> Bool

isPrime x = x == last (takeWhile (<= x) primes)

isPrime1 x = case l of [x] -> True
                       [] -> False
             where l = [p | (p, n) <- zip primes [1..x], p == x ]
                   
isPrime2 x = inList x primes
  where inList x (p:ps) = if x == p then True
                          else if x < p then False
                               else inList x ps

{-|
  Problem 32
  (**) Determine the greatest common divisor of two integer numbers.
  Use Euclid's algorithm.
  Example in Haskell:

  > [myGCD 36 63, myGCD (-3) (-6), myGCD (-3) 6]
  [9,3,3]
-}

myGCD :: Integer -> Integer -> Integer
myGCD a b = if b == 0 then abs a
            else myGCD b (a `mod` b)
                 

{-|
  Problem 33

   (*) Determine whether two positive integer numbers are coprime.
   Two numbers are coprime if their greatest common divisor equals 1.

   Example in Haskell:
   λ> coprime 35 64
   True
-}

coprime  :: Integer -> Integer -> Bool
coprime a b = myGCD a b == 1

{-|
  Problem 34
  (**) Calculate Euler's totient function phi(m).

  Euler's so-called totient function phi(m) is defined as the number of
  positive integers r (1 <= r < m) that are coprime to m.

  Example: m = 10: r = 1,3,7,9; thus phi(m) = 4. Note the special case: phi(1) = 1.

  Example in Haskell:
  λ> totient 10
  4
-}

totient :: Integer -> Integer
totient n = length [x | x <- [1..n], coprime x n]


{-|
  Problem 35

  (**) Determine the prime factors of a given positive integer.

  Construct a flat list containing the prime factors in ascending order.

  Example in Haskell:
  λ> primeFactors 315
  [3, 3, 5, 7]

  Problem 36

  Construct a list containing the prime factors and their multiplicity.

  Example in Haskell:
  λ> prime_factors_mult 315
  [(3,2),(5,1),(7,1)]
-}
primeFactorsList :: Integer -> [[Integer]]
primeFactors :: Integer -> [Integer]
prime_factors_mult :: Integer -> [(Integer,Integer)]

primeFactorsList n = let
      primes_less n = tail $ takeWhile (<= n) primes
      divide nn acc d = if (nn `mod` d == 0) then divide (nn `div` d) (acc ++ [d]) d
                        else acc
  in filter (not . null) $ map (divide n []) (primes_less n)
  
primeFactors n = concat $ primeFactorsList n

prime_factors_mult n = map (\l -> (head l, length l)) (primeFactorsList n)

{-|
  Problem 37
  (**) Calculate Euler's totient function phi(m) (improved).

  See problem 34 for the definition of Euler's totient function.
  If the list of the prime factors of a number m is known in the form
  of problem 36 then the function phi(m) can be efficiently calculated
  as follows: Let ((p1 m1) (p2 m2) (p3 m3) ...) be the list of prime
  factors (and their multiplicities) of a given number m.
  Then phi(m) can be calculated with the following formula:

  phi(m) = (p1 - 1) * p1 ** (m1 - 1) * 
           (p2 - 1) * p2 ** (m2 - 1) * 
           (p3 - 1) * p3 ** (m3 - 1) * ...

  Note that a ** b stands for the b'th power of a.

  Problem 38
  (*) Compare the two methods of calculating Euler's totient function.
  
  Prelude> totient 10090
  4032
  (0.05 secs, 16,489,504 bytes)
  Prelude> totient37 10090
  4032
  (0.21 secs, 154,336,456 bytes)
-}

prime_factors_mult :: Integer -> Integer
totient37 m = product [(p - 1) * p ^ (c - 1) | (p, c) <- prime_factors_mult m]

{-|
  Problem 39

  (*) A list of prime numbers.

  Given a range of integers by its lower and upper limit,
  construct a list of all prime numbers in that range.

  Example in Haskell:
  λ> primesR 10 20
  [11,13,17,19]
-}

primesR :: Integer -> Integer -> [Integer]
primesR min max = dropWhile (< min) $ takeWhile (<= max) primes

{-|
  Problem 40
  (**) Goldbach's conjecture.

  Goldbach's conjecture says that every positive even number greater
  than 2 is the sum of two prime numbers. Example: 28 = 5 + 23.
  It is one of the most famous facts in number theory that has not been
  proved to be correct in the general case. It has been numerically
  confirmed up to very large numbers (much larger than we can go with
  our Prolog system). Write a predicate to find the two prime numbers
  that sum up to a given even integer.

  Example in Haskell:
  λ> goldbach 28
  (5, 23)

  Problem 41
  (**) Given a range of integers by its lower and upper limit,
  print a list of all even numbers and their Goldbach composition.

  In most cases, if an even number is written as the sum of two
  prime numbers, one of them is very small. Very rarely, the primes
  are both bigger than say 50. Try to find out how many such
  cases there are in the range 2..3000.

  Example in Haskell:

  λ> goldbachList 9 20
  [(3,7),(5,7),(3,11),(3,13),(5,13),(3,17)]
  λ> goldbachList' 4 2000 50
  [(73,919),(61,1321),(67,1789),(61,1867)]

-}

goldbach :: Integer -> (Integer,Integer)
goldbachList Integer -> Integer -> [(Integer,Integer)]
goldbachList' Integer -> Integer -> Integer -> [(Integer,Integer)]

goldbach n = head [(x,y) | x <- pr, y <- pr, x+y==n]
  where pr = primesR 2 (n-2)

goldbachList n m = map goldbach $ dropWhile (<4) $ filter even [n..m]
goldbachList' n m i = filter (\(x,y) -> x > i && y > i) $ goldbachList n m










