import Data.List

{-|
  Problem 1
  (*) Find the last element of a list.
  (Note that the Lisp transcription of this problem is incorrect.)

  Example in Haskell:
  λ> myLast [1,2,3,4]
  4
  λ> myLast ['x','y','z']
  'z'
-}

getLast :: [a] -> a
getLast []     = error "empty!"
getLast [x]    = x
getLast (_:xs) = getLast xs

getLast1 :: [a] -> a
getLast1 = head . reverse

{-|
  Problem 2
  (*) Find the last but one element of a list.
  (Note that the Lisp transcription of this problem is incorrect.)

  Example in Haskell:
  λ> myButLast [1,2,3,4]
  3
  λ> myButLast ['a'..'z']
  'y'
-}

getButOne : [a] -> a
getButOne xs =
    if null xs || null (tail xs) then error "List too short!"
    else
        if null (tail (tail xs)) then head xs
        else getButOne (tail xs)

{-|
  Problem 3
  (*) Find the K'th element of a list. The first element in the list is number 1.

  Example in Haskell:
  λ> elementAt [1,2,3] 2
  2
  λ> elementAt "haskell" 5
  'e'
-}

elementAt :: [a] -> Int -> a
elementAt (x:_)  1 = x
elementAt (_:xs) n = elementAt xs (n-1)
elementAt _ _      = error "Index out of bounds"


{-|
  Problem 4
  (*) Find the number of elements of a list.

  Example in Haskell:
  λ> myLength [123, 456, 789]
  3
  λ> myLength "Hello, world!"
  13
-}

myLength :: [a] -> Int
myLength xs = myCount 0 xs where
    myCount n [] = n
    myCount n (_:xs) = myCount (n+1) xs

myLength1 :: [a] -> Int
myLength1 = foldr (\_ -> (+1)) 0

{-|
  Problem 5
  (*) Reverse a list.

  Example in Haskell:
  λ> myReverse "A man, a plan, a canal, panama!"
  "!amanap ,lanac a ,nalp a ,nam A"
  λ> myReverse [1,2,3,4]
  [4,3,2,1]
-}
myReverse :: [a] -> [a]
myReverse xs = myReverse_acc [] xs where
    myReverse_acc zs [] = zs
    myReverse_acc zs (x:xs) = myReverse_acc (x:zs) xs

{-|
  Problem 6
  (*) Find out whether a list is a palindrome.
  A palindrome can be read forward or backward; e.g. (x a m a x).

  Example in Haskell:
  λ> isPalindrome [1,2,3]
  False
  λ> isPalindrome "madamimadam"
  True
  λ> isPalindrome [1,2,4,8,16,8,4,2,1]
  True
-}

isPalindrome :: (Eq t) => [t] -> Bool
isPalindrome [] = True
isPalindrome [_] = True
isPalindrome xs = (head xs == last xs) &&
                  (isPalindrome $ init $ tail xs)

isPalindrome1 :: (Eq t) => [t] -> Bool
isPalindrome1 xs = xs == myReverse xs

{-|
  Problem 7
  (**) Flatten a nested list structure.

  Transform a list, possibly holding lists as elements into a `flat'
  list by replacing each list with its elements (recursively).

  Example in Haskell:
  We have to define a new data type, because lists in Haskell are homogeneous.

   data NestedList a = Elem a | List [NestedList a]

   λ> flatten (Elem 5)
   [5]
   λ> flatten (List [Elem 1, List [Elem 2, List [Elem 3, Elem 4], Elem 5]])
   [1,2,3,4,5]
   λ> flatten (List [])
   []
-}

data NestedList a = Elem a | List [NestedList a]

flatten0 :: NestedList a -> [a]
flatten0 (Elem x) = [x]
flatten0 (List x) = concatMap flatten0 x

flatten1 :: NestedList a -> [a]
flatten1 (List []) = []
flatten1 (Elem x) = [x]
flatten1 (List (x:xs)) = flatten1 x ++ flatten1 (List xs)

flatten2 :: NestedList a -> [a]
flatten2 (Elem x) = return x
flatten2 (List x) = flatten2 =<< x

{-|
  Problem 8
  (**) Eliminate consecutive duplicates of list elements.

  If a list contains repeated elements they should be replaced
  with a single copy of the element.
  The order of the elements should not be changed.

  Example in Haskell:
  λ> compress "aaaabccaadeeee"
  "abcade"
-}

compress :: Eq a => [a] -> [a]
compress [] = []
compress (x:xs) = x : (compress $ dropWhile (== x) xs)

{-|
  Problem 9
  (**) Pack consecutive duplicates of list elements into sublists.
  If a list contains repeated elements they should be placed
  in separate sublists.

  Example in Haskell:
  λ> pack ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a',  'a', 'd', 'e', 'e', 'e', 'e']
  ["aaaa","b","cc","aa","d","eeee"]
-}

pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x : takeWhile (==x) xs) : pack (dropWhile (==x) xs)

{-|
  Problem 10
  (*) Run-length encoding of a list.
  Use the result of problem P09 to implement the so-called run-length encoding
  data compression method. Consecutive duplicates of elements are encoded as
  lists (N E) where N is the number of duplicates of the element E.
  Example:
  encode "aaaabccaadeeee"
  [(4,'a'),(1,'b'),(2,'c'),(2,'a'),(1,'d'),(4,'e')]
-}
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x : takeWhile (==x) xs) : pack (dropWhile (==x) xs)

encode :: Eq a => [a] -> [(Int, a)]
encode [] = []
encode xs = [(n, x) | (n, x) <- map (\l -> (length l, head l)) $ pack xs ]


{-|
  Problem 11
  (*) Modified run-length encoding.
  Modify the result of problem 10 in such a way that if an element
  has no duplicates it is simply copied into the result list.
  Only elements with duplicates are transferred as (N E) lists.
  Example:
  encodeModified "aaaabccaadeeee"
  [Multiple 4 'a',Single 'b',Multiple 2 'c',  Multiple 2 'a',Single 'd',Multiple 4 'e']
-}

data ListItem a = Single a | Multiple Int a  deriving (Show)

encode :: (Eq a) => [a] -> [(Int, a)]
encode [] = []
encode xs = [(n, x) | (n, x) <- map (\l -> (length l, head l)) $ group xs ]

encodeModified :: (Eq a) => [a] -> [ListItem a]
encodeModified = map encodeHelper . encode
                 where encodeHelper (1,x) = Single x
                       encodeHelper (n,x) = Multiple n x

encodeModified1 :: (Eq a) =>  [a] -> [ListItem a]
encodeModified1 xs = [y | x <- group xs,
                          let y = if (length x) == 1
                                  then Single (head x)
                                  else Multiple (length x) (head x)]


{-|
  Problem 12
  (**) Decode a run-length encoded list.
  Given a run-length code list generated as specified in problem 11.
  Construct its uncompressed version.
  Example in Haskell:
  decodeModified [Multiple 4 'a',Single 'b',Multiple 2 'c',
                  Multiple 2 'a',Single 'd',Multiple 4 'e']
  "aaaabccaadeeee"
-}

decodeHelper :: ListItem a -> [a]
decodeHelper a = case a of
                   (Single x)     -> [x]
                   (Multiple n x) -> replicate n x

decodeModified :: [ListItem a] -> [a]
decodeModified = concatMap decodeHelper

decodeModified1 :: [ListItem a] -> [a]
decodeModified1 a = decodeHelper =<< a

{-|
  Problem 13
  (**) Run-length encoding of a list (direct solution).
  Implement the so-called run-length encoding data compression
  method directly. I.e. don't explicitly create the sublists
  containing the duplicates, as in problem 9, but only count them.
  As in problem P11, simplify the result list by replacing
  the singleton lists (1 X) by X.

  Example:
  encodeDirect "aaaabccaadeeee"
  [Multiple 4 'a',Single 'b',Multiple 2 'c',
   Multiple 2 'a',Single 'd',Multiple 4 'e']
-}

(Eq a) => [a] -> [ListItem a]
encodeDirect [] = []
encodeDirect xs = map (\l -> if length l == 1 then Single (head l)
                             else Multiple (length l) (head l)) $ group xs

encodeDirect1 :: (Eq a) => [a] -> [ListItem a]
encodeDirect1 []     = []
encodeDirect1 (x:xs) =
  let (grp, rest) = span (==x) xs in
    ifSingle (Multiple (1 + length grp) x) : encodeDirect1 rest
  where ifSingle a = case a of
          (Multiple 1 x) -> (Single x)
          _ -> a

{-|
  Problem 14
  (*) Duplicate the elements of a list.

  Example in Haskell:
  λ> dupli [1, 2, 3]
  [1,1,2,2,3,3]
-}


dupli1 :: [a] -> [a]
dupli1 [] = []
dupli1 (x:xs) = x:x:dupli1 xs

dupli2 :: [a] -> [a]
dupli2 xs = xs >>= (\x -> [x,x])

{-|
  Problem 15
  (**) Replicate the elements of a list a given number of times.
  Example:
  * (repli '(a b c) 3)
  (A A A B B B C C C)

  Example in Haskell:
  λ> repli "abc" 3
  "aaabbbccc"
-}

repli1 :: [a] -> Int -> [a]
repli1 xs n = concatMap (replicate n) xs

repli2 :: [a] -> Int -> [a]
repli2 xs n = xs >>= replicate n


-- Problem 16 (**) Drop every N'th element from a list.

dropEvery :: [a] -> Int -> [a]
dropEvery xs n = helper xs n
    where helper [] _ = []
          helper (x:xs) 1 = helper xs n
          helper (x:xs) k = x : helper xs (k-1)

dropEvery1 :: [a] -> Int -> [a]
dropEvery1 [] _ = []
dropEvery1 xs count = (take (count-1) xs) ++ dropEvery1 (drop count xs) count

{-|
  Problem 17
  (*) Split a list into two parts; the length of the first part is given.
  Do not use any predefined predicates.

  Example in Haskell:
  λ> split "abcdefghik" 3
  ("abc", "defghik")
-}

split1 :: [a] -> Int -> ([a], [a])
split1 xs n = (take n xs, drop n xs)

-- splitAt :: Int -> [a] -> ([a], [a]), so we need flip
split2 :: [a] -> Int -> ([a], [a])
split2 = flip splitAt

{-|
  Problem 18
  (**) Extract a slice from a list.
  Given two indices, i and k, the slice is the list containing the elements
  between the i'th and k'th element of the original list (both limits included).
  Start counting the elements with 1.

  Example in Haskell:
  λ> slice ['a','b','c','d','e','f','g','h','i','k'] 3 7
  "cdefg"
-}

slice :: [a] -> Int -> Int -> [a]
slice xs start fin = take (fin-start+1) $ drop (start-1) xs

slice1 :: [a] -> Int -> Int -> [a]
slice1 xs start fin = [x | (x,j) <- zip xs [1..fin], start <= j]

{-|
  Problem 19
  (**) Rotate a list N places to the left.
  Hint: Use the predefined functions length and (++).

  Examples in Haskell:
  λ> rotate ['a','b','c','d','e','f','g','h'] 3
  "defghabc"

  λ> rotate ['a','b','c','d','e','f','g','h'] (-2)
  "ghabcdef"
-}

rotate :: [a] -> Int -> Int -> [a]
rotate xs n = if n >= 0 then (drop n xs) ++ (take n xs)
              else (drop z xs) ++ (take z xs)
                   where z = n + length xs

rotate1 :: [a] -> Int -> Int -> [a]
rotate1 xs n = drop nn xs ++ take nn xs
    where nn = n `mod` length xs


{-|
  Problem 20
  (*) Remove the K'th element from a list.

  Example in Prolog:
  ?- remove_at(X,[a,b,c,d],2,R).
  X = b
  R = [a,c,d]

  Example in Lisp:
  * (remove-at '(a b c d) 2)
  (A C D)

  Note that this only returns the residue list,
  while the Prolog version also returns the deleted element.

  Example in Haskell:
  λ> removeAt 2 "abcd"
  ('b',"acd")
-}

removeAt :: Int -> [a] -> (a, [a])
removeAt n xs = (xs !! (n-1), (take (n-1) xs) ++ drop n xs)

removeAt1 :: Int -> [a] -> (a, [a])
removeAt1 n xs | n > 0 && n <= length xs = (Just (xs !! index), take index xs ++ drop n xs)
               | otherwise = (Nothing, xs)
               where index = n - 1

removeAt3 :: (Enum a) => Int -> [a] -> (a, [a])
removeAt3 n xs = ((xs !! (n-1)), [ x | (i, x) <- zip [1..] xs, i /= n ])

removeAt4 :: Int -> [a] -> (a, [a])
removeAt4 1 (x:xs) = (x, xs)
removeAt4 n (x:xs) = (l, x:r)
    where (l, r) = removeAt4 (n - 1) xs

{-|
  Problem 21
  Insert an element at a given position into a list.

  Example in Haskell:
  λ> insertAt 'X' "abcd" 2
  "aXbcd"
-}

insertAt :: a -> [a] -> Int -> [a]
insertAt x xs n =
    let (front, tail) = splitAt n xs in front ++ x:tail

{-|
   Problem 22.
   Create a list containing all integers within a given range.

   Example in Haskell:
   λ> range 4 9
   [4,5,6,7,8,9]
-}

range :: (Int a) => a -> a -> [a]
range x y = [x..y]

{-|
   Problem 23
   Extract a given number of randomly selected elements from a list.

   Example in Haskell:
   λ> rnd_select "abcdefgh" 3 >>= putStrLn
   eda
-}

rnd_select :: [a] -> Int -> [a]
